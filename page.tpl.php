<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
 
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<!-- Main Container Starts Here -->
<div class="main-container">
	<div class="container">
		<div class="header">
			<div class="logo">
				<?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
			</div>
			<div class="slogan">
				<?php if ($site_slogan) { ?><?php print $site_slogan ?><?php } ?>
			</div>
			<div class="search-box">
				<?php if ($search_box) { ?><?php print $search_box ?><?php } ?>
			</div>
		</div>
		<div class="leftbar">
			<?php if (isset($secondary_links)) { ?>
				<?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?>
			<?php } ?>
	        <?php if (isset($primary_links)) { ?>
				<?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?>
			<?php } ?>
			<?php print $left; ?>
		</div>
		<div class="right-area">
			<div class="top-image">
				<div class="site-name">
					<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
				</div>
			</div>
			<div class="content-area">
	 
				<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
				  <div id="main">
					<?php print $breadcrumb ?>
					<h1 class="title"><?php print $title ?></h1>
					<div class="tabs"><?php print $tabs ?></div>
					<?php print $help ?>
					<?php print $messages ?>
					<?php print $content; ?>
					<?php print $feed_icons; ?>
				  </div>
			</div>
		</div>
	</div>
</div>
<!-- Mian Container Ends Here -->
<div class="footer">
	<div class="footer-info">
	<?php print $footer_message ?>
	Powered by Drupal, Design and Developed by <a href="http://www.cmswebsiteservices.com">CMS Website Services</a>, LLC 
	</div>
</div>
<?php print $closure ?>
</body>
</html>
